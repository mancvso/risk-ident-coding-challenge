addSbtPlugin("ch.epfl.lamp" % "sbt-dotty" % "0.3.4")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

addSbtPlugin("com.sksamuel.scapegoat" %% "sbt-scapegoat" % "1.0.9")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.4.1")