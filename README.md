# Coding Challenge

## Quickstart

A convenient docker image is published in Gitlab.com registry for inmediate consumption

```bash
docker run --net=host -v $(pwd)/logs:/logs -e LINES=1000 registry.gitlab.com/mancvso/risk-ident-coding-challenge:1.0.0
```

Otherwise, to run fresh from source code
```bash
LOGS_FOLDER=$(pwd)/logs LOG_LEVEL=trace sbt clean run
```

Available log levels are `trace`, `debug`, `info`, `warning`, `error`.

## Changelog

### v1.0.0
What has been improved
- More clean architecture, separated business from domain annd more meaninful names
- [fix] Fixed a test not passing on windows machines
- Added logging (an implementation interoperable with scala-logging and slf4s)
- Added HTTP service layer
- Enhanced error handling
- Display time elapsed in the whole process
- Docker image for convenience and automated deploy

## Building


### Prepare universal binaries
```
sbt clean stage
```

### Build Docker image
```bash
docker build -t registry.gitlab.com/mancvso/risk-ident-coding-challenge:1.0.0 .
```

## Runnning

When running from the docker image, is necessary to give access to the CSV generator test server. The simplest way is to attach the container to the host network. Also, addinng a volume for `/logs` allows to retrieve log files after termination. The amount of articles and the server url can be set during runtime.

Complete command

```bash
docker run --net=host -v $(pwd)/logs:/logs -e LINES=1000 -e "SERVER=https://someotherhost:8080" -e LOG_LEVEL=info registry.gitlab.com/mancvso/risk-ident-coding-challenge:1.0.0
```

Minimal command

```bash
docker run --net=host -v $(pwd)/logs:/logs registry.gitlab.com/mancvso/risk-ident-coding-challenge:1.0.0
```

### Known issues

Akka has a false positive when shutting down reactive HTTP clients 

This text can be ignored from the output and it does not interfere with the correct functioning of the software.
```
[ERROR] [10/28/2019 01:09:29.282] [mancvso-akka.actor.default-dispatcher-21] [akka.actor.ActorSystemImpl(mancvso)] Outgoing request stream error (akka.http.impl.engine.client.pool.NewHostConnectionPool$HostConnectionPoolStage$$anon$1$Slot$$anon$2: Pool slot was shut down)
```

## Problem Statement

As marketing representative I would like to transform and forward the current product list of our online shops to an external service provider.

The input is the current list of articles in CSV format. An article is a variant of a product and a product may have multiple articles (e.g. different colour shirts). In the input the articles for a particular product will always be next to each other.

For each product, **only the cheapest article that is in stock should be included in the result**. The **details** in the output format should come from this article.

The **stock** in the output should be the sum of the stocks of all articles of the product.

If no articles of a product are in stock, then the product should not appear in the output at all.

If multiple articles have the same price, the first one in the input should be used.

Your solution should do the following:

- Retrieve current list of articles from an HTTP endpoint
- Do a PUT of the results to an HTTP endpoint

You are encouraged to write the code in any language you are comfortable with and in a way that you would describe as "production ready".

### Test Service

A test service is provided for you to test your implementation against.

- Start: `java -jar coding-challenge.jar`

#### Description of the Download endpoint
- HTTP GET `/articles/:lines`
 - `lines` specifies the number of returned articles
- Response Body: the CSV file

#### Description of the Upload endpoint
- HTTP PUT `/products/:lines`, Content-Type: text/csv
 - `lines` refers to the number of underlying articles
- Response Code: 200 if all entries have been successfully processed

#### Format of the source file
- separator: pipe (|)
- Line separator: LF (\n)
- Line 1: header / column names in German
- Columns: id|produktId|name|beschreibung|preis|bestand (String|String|String|String|Float|Int)
- Note: the delimiter will never be present in the values

#### Format of the destination file
- separator: pipe (|)
- Line separator: LF (\n)
- Line 1: header / column names in German
- Columns: produktId|name|beschreibung|preis|summeBestand (String|String|String|Float|Int)

#### German column name translation
- "beschreibung" (german) means "description"
- "preis" (german) means "price"
- "bestand" (german) means "stock"
- "summeBestand" (german) means "sum of stocks"

#### Price formats
Prices have one or two decimal places and "." as separator between Euro and Cent without currency symbol.

For example: 12.13, 42.03 or 90.0

