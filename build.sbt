val dottyVersion = "0.19.0-RC1"
val scala212Version = "2.12.10"

scapegoatVersion in ThisBuild := "1.3.8"

enablePlugins(JavaAppPackaging)

lazy val root = project
  .in(file("."))
  .settings(
    name := "mancvso-article-parser",
    version := "1.0.0",
    scalaVersion := "2.12.10",

    scalacOptions := Seq("-unchecked", "-deprecation"),

    resolvers += Resolver.sonatypeRepo("releases"),

    libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.10",

    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.26",

    libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.26" % Test,

    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test,
    
    libraryDependencies += "com.github.melrief" %% "purecsv" % "0.1.1",

    libraryDependencies += "org.tinylog" % "tinylog-api-scala" % "2.0.0",

    libraryDependencies += "org.tinylog" % "tinylog-impl" % "2.0.0",

    // Some libraries are not ready yet
    //libraryDependencies := libraryDependencies.value.map(_.withDottyCompat(scalaVersion.value)),
    
    // To make the default compiler and REPL use Dotty
    //scalaVersion := dottyVersion,

    // To cross compile with Dotty and Scala 2
    // crossScalaVersions := Seq(dottyVersion, scala212Version)
  )
