FROM openjdk:8u111-jdk-alpine
RUN apk add --no-cache bash
RUN mkdir /logs
ENV LOGS_FOLDER /logs
ENV LOG_LEVEL info
VOLUME /tmp
COPY target/universal/stage /app
WORKDIR /app
CMD ["/app/bin/mancvso-article-parser"]