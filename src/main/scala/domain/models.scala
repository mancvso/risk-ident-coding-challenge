package mancvso.domain

import scala.util.{Failure, Try, Success}

// Represents input data
final case class Article(
  id: String,
  produktId: String,
  name: String,
  beschreibung: String,
  preis: Price,
  bestand: Long)

final case class Price(p:Float) extends AnyVal {
  // The correctness of this method requires a call to Util.ensureLocale
  override def toString() = "%.2f".format(p).replace(",", ".")

  def nearEquals(that:Price) :Boolean = {
    // Specification of the task says prices are at most two decimals
    ((this.p - that.p).abs < 0.001)
  }

  def <(that:Price) :Boolean = {
    this.p < that.p
  }
}

// Represents output data
final case class ArticleDisplay(
  produktId: String,
  name: String,
  beschreibung: String,
  preis: Price,
  summeBestand: Long)

// Represents a group of articles of the same product
final case class Product(
  id: String,
  articles: Seq[Article]) {
  def withStock() = Product(id, articles = articles.filter( a => a.bestand > 0) )
  def cheapest(): Option[Article] = {
    articles.reduceLeftOption( (a1, a2) => {
      if(a1.preis < a2.preis){
        a1
      } else if(a1.preis nearEquals a2.preis){
        // Preserve order then prices are equal
        if( articles.indexOf(a1) < articles.indexOf(a2)){
          a1
        } else {
          a2
        }
      } else {
        a2
      }
    })
  }
  def stock() :Long = articles.foldLeft(0L)( (sum, article) => sum + article.bestand)
  
  def display() : Option[ArticleDisplay] = {
    val cheapest = this.cheapest()
    val stock = this.stock()
    if( 0 == stock ) {
        None
    } else {
        cheapest.map( c => {
            ArticleDisplay(
                c.produktId,
                c.name,
                c.beschreibung,
                c.preis,
                stock
            )
        })
    }
  }
}