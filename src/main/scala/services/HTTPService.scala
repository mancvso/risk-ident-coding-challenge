package mancvso.service

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer

import org.tinylog.Logger
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.Future
import scala.util.{Success, Failure, Try}
import akka.http.scaladsl.HttpExt
import scala.concurrent.ExecutionContext

trait GenericHTTPService {
  def sendResult(data: String, serverAddress: String, lines: Long, timeout: FiniteDuration) :Future[Try[String]]
  def getArticleData(serverAddress: String, timeout: FiniteDuration, lines: Long) :Future[String]
  def cleanup() :Unit
}

trait AkkaHTTPService {
  val http: HttpExt
  def singleRequest: HttpRequest => Future[HttpResponse]
  implicit val system: ActorSystem
  implicit val materializer :ActorMaterializer
  implicit val executionContext :ExecutionContext
}

class HTTPService extends GenericHTTPService with AkkaHTTPService {
  implicit val system = ActorSystem("mancvso")
  implicit val materializer = ActorMaterializer(namePrefix=Option("mancvso"))
  implicit val executionContext = scala.concurrent.ExecutionContext.global

  val http: HttpExt = Http(system)
  def singleRequest: HttpRequest => Future[HttpResponse] = http.singleRequest(_)

  // Send result data to server
  def sendResult(data: String, serverAddress: String, lines: Long, timeout: FiniteDuration) = {
    val url = s"$serverAddress/products/$lines"
    val bytesBody = data.getBytes()
    Logger.debug(s"Making PUT request to $url with ${bytesBody.size} bytes")
    Logger.trace(data)
    singleRequest(
      HttpRequest(
        HttpMethods.PUT,
        url,
        // target server does not accept the in-lined charset of ContentTypes.`text/csv(UTF-8)`
        entity = HttpEntity(ContentType.WithMissingCharset(MediaTypes.`text/csv`), bytesBody)
      )
    ) map { res => {
        if(res.status == StatusCodes.OK || res.status == StatusCodes.NotAcceptable) {
          res.entity.toStrict(timeout)
        } else {
          Future.failed(new RuntimeException(s"The server responded with a status code of ${res.status.intValue}:${res.status.reason}"))
        }
      }
    } flatMap { unwraped => unwraped} map { _.data.utf8String } map { body =>
      if(body.endsWith("result is correct")) {
        Success(body)
      } else {
        Failure(new RuntimeException(body))
      }
    }
  }

  def getArticleData(serverAddress: String, timeout: FiniteDuration, lines: Long) = {
    val url = s"$serverAddress/articles/$lines"
    Logger.debug(s"Requesting articles from $url with timeout=$timeout ")
    singleRequest(HttpRequest(uri = url)) map { res =>
      Logger.debug(s"Got status ${res.status.intValue}")
      if(res.status == StatusCodes.OK){
        res.entity.toStrict(timeout)
      } else {
        Future.failed(new RuntimeException("Unexpected response from server"))
      }
    } flatMap { unwraped => unwraped } map { _.data.utf8String }
  }

  def cleanup() {
    Logger.info("Closing all resources. Pool slot will be shut down.")
    http.shutdownAllConnectionPools() andThen {
      case all => materializer.shutdown; system.terminate()
    }
  } 


}