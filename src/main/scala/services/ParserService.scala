package mancvso.service

import scala.util.{Try, Success, Failure}

import purecsv.safe._
import purecsv.safe.converter.StringConverter

import org.tinylog.Logger

import mancvso.domain._
import mancvso.util.CollectionUtils

trait GenericParserService {
  def hasHeader(s :String) :Boolean
  def singleLine(s: String) :List[scala.util.Try[Article]]
  def multiLine(s: String, trim: Boolean = false, hasHeader: Boolean = false) :List[scala.util.Try[Article]]
  def processInput(in: String) :Seq[ArticleDisplay]
  def processOutput(out:Seq[ArticleDisplay]) :String



}

object ParserService extends GenericParserService {

  private def ltrim(s: String) = s.replaceAll("^\\s+", "")
  private def rtrim(s: String) = s.replaceAll("\\s+$", "")
  private def superTrim(s: String) = ltrim(rtrim(s))

  // It is necesary to use a custom class to control the format
  implicit val dateTimeStringConverter = new StringConverter[Price] {
    override def tryFrom(str: String): Try[Price] = Try(Price(str.toFloat))
    override def to(p: Price): String = p.toString()
  }

  // Hardcoded because it is validad in the bussiness logic
  final val SCHEMA_V1 = "id|produktId|name|beschreibung|preis|bestand"
  final val SCHEMA_OUT_V1 = "produktId|name|beschreibung|preis|summeBestand"

  def hasHeader(s :String) = s != null && superTrim(s).startsWith(SCHEMA_V1) 
  
  private def failure(msg: String) = {
    Logger.error(msg)
    List(Failure.apply[Article](new RuntimeException(msg)))
  }

  def singleLine(s: String) :List[scala.util.Try[Article]] = {
    if(Option(s).nonEmpty){
      CSVReader[Article].readCSVFromString(superTrim(s), '|')
    } else {
      failure("An empty string was provided to the parser in single line mode")
    }
  }

  def multiLine(s: String, trim: Boolean = false, hasHeader: Boolean = false) :List[scala.util.Try[Article]] = {
    Logger.debug(s"Parsing with trim=$trim and hasHeader=$hasHeader")
    if(trim && Option(s).nonEmpty){
      CSVReader[Article].readCSVFromString(superTrim(s), '|', hasHeader)
    } else if(Option(s).nonEmpty){
      CSVReader[Article].readCSVFromString(s, '|', hasHeader)
    } else {
      failure("An empty string was provided to the parser")
    }
  }

  // Ensembles case classes from CSV string
  def processInput(in: String) :Seq[ArticleDisplay] = {

    Logger.debug("Process CSV chunk")

    val results = ParserService.multiLine(in, hasHeader = true, trim = true)
    
    val invalidResults = results.collect {
      case Failure(exception) => exception
    }

    val validResults = results.collect {
      case Success(s) => s
    }

    Logger.debug(s"Got ${validResults.size} valid articles")
    
    if(invalidResults.nonEmpty) {
      Logger.error(s"${invalidResults.size} articles could not be parsed. The first three exceptions are:")
      Logger.error(invalidResults.take(3))
    }
    
    /* Standard Scala groupBy function does not preserve key ordering
      and the server expects productId in the same order given */
    val products = CollectionUtils.orderedGroupBy(validResults)( a => a.produktId )
    .map( el => Product(el._1, el._2) )

    Logger.debug(s"Generated ${products.size} products")

    products.map( p => p.withStock.display ).flatten[ArticleDisplay].toSeq
  }

  def processOutput(out:Seq[ArticleDisplay]) :String = {
    Logger.debug("Generate CSV chunk")
    // On windiows systems, the line separator needs to be fixed to meet specification
    val outt = out.toCSV("|").replace("\"", "").replace("\r\n", "\n")
    Logger.trace(outt)
    ParserService.SCHEMA_OUT_V1 + "\n" + outt
  }
}