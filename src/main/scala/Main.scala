package mancvso

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

import org.tinylog.Logger

import mancvso.service.ParserService
import mancvso.service.HTTPService
import mancvso.domain.ArticleDisplay
import akka.http.scaladsl.Http

object Main {

  implicit val executionContext = scala.concurrent.ExecutionContext.global

  // Allow to specify constants trough env
  val lines = sys.env.get("LINES").map(_.toLong).getOrElse(5000L)
  val serverAddressOption = sys.env.get("SERVER")
  val serverAddress = serverAddressOption.getOrElse("http://localhost:8080")

  val httpServiceImp = new HTTPService()

  Logger.info(s"Starting process with lines=$lines and server=$serverAddress")

  def main(args: Array[String]): Unit = {

  val startTime = System.currentTimeMillis()

    def prepareResult(articleData: String) = {
      ParserService.processOutput(ParserService.processInput(articleData))
    }
    
    // A large timeout is required when dealing with a sample data of 100000
    val timeout = Math.max(lines, 300).millis

    val process = for {
      articleData <- httpServiceImp.getArticleData(serverAddress, timeout, lines)
      putResponse <- httpServiceImp.sendResult( prepareResult(articleData), serverAddress, lines, timeout)
    } yield putResponse

    // This will have Failure(_) when GET fails
    process onComplete {
      case Failure(ex) => Logger.error(s"Could not fetch article data $ex")
      case Success(data) => Logger.trace("Both requests were sent")
    }

    // This will have Failure(_) when PUT fails
    process map {
      case Success(result) => Logger.info(s"Process ended correctly. Server responded with $result")
      case Failure(err) => Logger.error(s"Article data rejected $err")
    } andThen {
      case all => {
        Logger.info(s"Process completed in ${System.currentTimeMillis() - startTime} [ms]")
        httpServiceImp.cleanup()
      }
    }
  }
}
