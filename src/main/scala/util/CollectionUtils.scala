package mancvso.util

import scala.annotation.tailrec
object CollectionUtils {
  // This methods resembles a Collection.orderBy but preserving the order of keys
  // from https://stackoverflow.com/a/29267135 modified to fit Seq instead of Traversable
  def orderedGroupBy[T, P](seq: Seq[T])(f: T => P): Seq[(P, Seq[T])] = {
    @tailrec
    def accumulator(seq: Seq[T], f: T => P, res: List[(P, Seq[T])]): Seq[(P, Seq[T])] = seq.headOption match {
      case None => res.reverse.toSeq
      case Some(h) => {
        val key = f(h)
        val subseq = seq.takeWhile(f(_) == key)
        accumulator(seq.drop(subseq.size), f, (key -> subseq) :: res)
      }
    }
    accumulator(seq, f, Nil)
  }
}