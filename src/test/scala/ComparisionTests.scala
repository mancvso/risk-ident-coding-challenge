package mancvso.test

import scala.util.{Try, Success, Failure}
import org.scalatest.{FlatSpec, Matchers}

import mancvso.domain._

class ComparisionTests extends FlatSpec with Matchers {

  it should "get the cheapest article variation in a Product category" in {
    val pid = "P-yhsgty"
    val cheapestArticle = Article("A-yheutdg", pid, "name name", "description", Price(40.70f), 10)
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 100),
        Article("A-yheutdg", pid, "name name", "description", Price(45.73f), 100),
        cheapestArticle
      )
    )

    p.cheapest shouldBe a [Some[_]]
    p.cheapest.get shouldEqual cheapestArticle
  }

  it should "show only articles with stock" in {
    val pid = "P-yhsgty"
    val articleWithoutStock = Article("A-yheutdg", pid, "name name", "description", Price(45.70f), 0)
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 100),
        Article("A-yheutdg", pid, "name name", "description", Price(45.73f), 100),
        articleWithoutStock
      )
    )
    p.withStock.articles.size shouldEqual 2
    p.withStock.articles shouldNot contain(articleWithoutStock)
  }

  it should "show the cheapest article with stock" in {
    val pid = "P-yhsgty"
    val cheapestWithStock = Article("A-yheutdg", pid, "name name", "description", Price(15.70f), 1)
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 100),
        Article("A-yheutdg", pid, "name name", "description", Price(10.73f), 0),
        cheapestWithStock
      )
    )
    p.withStock.cheapest shouldBe a [Some[_]]
    p.withStock.cheapest.get shouldEqual cheapestWithStock
  }

  it should "do not show product if no articles are in stock" in {
    val pid = "P-yhsgty"
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 0),
        Article("A-yheutdg", pid, "name name", "description", Price(10.73f), 0),
        Article("A-yheutdg", pid, "name name", "description", Price(10.73f), 0)
      )
    )
    p.withStock.articles.size shouldEqual 0
  }

  it should "show product stock as sum of all articles stock" in {
    val pid = "P-yhsgty"
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 5),
        Article("A-yheutdg", pid, "name name", "description", Price(10.73f), 10),
        Article("A-yheutdg", pid, "name name", "description", Price(10.73f), 15)
      )
    )
    p.withStock.stock shouldEqual 30
  }

  it should "show first input article if multiple have the same price" in {
    val pid = "P-yhsgty"
    val firstWithSamePrice = Article("A-yheutdg", pid, "name first", "description", Price(10.73f), 10)
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 5),
        firstWithSamePrice,
        Article("A-yheutdg", pid, "name second", "description", Price(10.73f), 15)
      )
    )
    p.withStock.cheapest shouldBe a [Some[_]]
    p.withStock.cheapest.get shouldEqual firstWithSamePrice

  }
}