package mancvso.test

import scala.util.{Try, Success, Failure}
import org.scalatest.{FlatSpec, Matchers}

import mancvso.domain._

class DisplayTests extends FlatSpec with Matchers {

  it should "Display only one article data in a series of articles" in {
    val pid = "P-yhsgty"
    val aid = "A-yheutdg"
    val aname = "cheapest article name"
    val ades = "cheapest article description"
    val aprice = Price(45.70f)
    val cheapestArticle = Article(aid, pid, aname, ades, aprice, 100)
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 100),
        Article("A-yheutdg", pid, "name name", "description", Price(45.73f), 100),
        cheapestArticle
      )
    )
    
    val result = p.withStock.display
    result shouldBe a [Some[_]]
    result.get.produktId shouldEqual pid
    result.get.name shouldEqual aname
    result.get.beschreibung shouldEqual ades
    result.get.preis shouldEqual aprice
    result.get.summeBestand shouldEqual p.stock
  }

  it should "Do not display articles without stock" in {
    val pid = "P-yhsgty"
    val aid = "A-yheutdg"
    val aname = "cheapest article name"
    val ades = "cheapest article description"
    val aprice = Price(45.70f)
    val cheapestArticle = Article(aid, pid, aname, ades, aprice, 0)
    val p = Product(
      id = pid,
      articles = List(
        Article("A-yhetdg", pid, "name name", "description", Price(45.78f), 0),
        Article("A-yheutdg", pid, "name name", "description", Price(45.73f), 0),
        cheapestArticle
      )
    )
    
    p.withStock.display shouldEqual None
  }

}