package mancvso.test

import scala.util.{Try, Success, Failure}
import org.scalatest.{FlatSpec, Matchers}

import mancvso.service.ParserService

class ParserTests extends FlatSpec with Matchers {

  it should "match validated output" in {
    val in = ParserService.processInput(Data.ten)
    val out = ParserService.processOutput(in)
    out.size shouldEqual Data.validatedTen().size
    out shouldEqual Data.validatedTen()
  }

}