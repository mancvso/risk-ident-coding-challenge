package mancvso.test

import org.scalatest.{FlatSpec, Matchers}
import scala.util.{Try, Success, Failure}

import mancvso.domain._

class PriceTests extends FlatSpec with Matchers {

  it should "detect two equal prices" in {
    val p1 = Price(5.78f)
    val p2 = Price(5.780f)
    p1 shouldEqual p2
  }

  it should "select the correct lesser Price" in {
    val p1 = Price(5.78f)
    val p2 = Price(4.78f)
    (p1 < p2 ) shouldEqual false
  }
}