package mancvso.test

import scala.util.{Try, Success, Failure}
import scala.concurrent.duration._
import scala.concurrent.Future

import org.scalatest.{FlatSpec, Matchers}

import mancvso.service.HTTPService

import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures

import akka.actor.ActorSystem
import akka.testkit.TestKitBase
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.ResponseEntity
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.ContentTypes

abstract class TestKitSpec(val system: ActorSystem)
    extends FlatSpec
    with Matchers
    with TestKitBase
    with BeforeAndAfterAll
    with ScalaFutures {

  override def afterAll() {
    system.terminate()
  }
}

class TestNotAvailableServiceHTTPService extends HTTPService {
    override def singleRequest: HttpRequest => Future[HttpResponse] = (req: HttpRequest) => Future.failed(new RuntimeException("Not available server"))
}

class TestMockDataHTTPService extends HTTPService {
    override def singleRequest: HttpRequest => Future[HttpResponse] = (req: HttpRequest) => Future.successful(HttpResponse(
        status = StatusCodes.OK,
        entity =  HttpEntity(ContentTypes.`text/csv(UTF-8)`, Data.ten))
      )
}

class TestFailingServerHTTPService extends HTTPService {
    override def singleRequest: HttpRequest => Future[HttpResponse] = (req: HttpRequest) => Future.successful(HttpResponse(
        status = StatusCodes.InternalServerError
      )
    )
}

class TestNotAcceptingHTTPService extends HTTPService {
    override def singleRequest: HttpRequest => Future[HttpResponse] = (req: HttpRequest) => Future.successful(HttpResponse(
        status = StatusCodes.NotAcceptable,
        entity =  HttpEntity(ContentTypes.`text/plain(UTF-8)`, "Response not accepted"))
      )
}

class TestSuccesfulHTTPService extends HTTPService {
    override def singleRequest: HttpRequest => Future[HttpResponse] = (req: HttpRequest) => Future.successful(HttpResponse(
        status = StatusCodes.OK,
        entity =  HttpEntity(ContentTypes.`text/plain(UTF-8)`, "result is correct"))
      )
}

class TestSuccesfulHTTPServiceWithDifferentResponseBody extends HTTPService {
    override def singleRequest: HttpRequest => Future[HttpResponse] = (req: HttpRequest) => Future.successful(HttpResponse(
        status = StatusCodes.OK,
        entity =  HttpEntity(ContentTypes.`text/plain(UTF-8)`, "body text is used also to validate correct approval"))
      )
}


class HTTPTests extends TestKitSpec(ActorSystem("macvso-test")) {

    val requestTimeout = 300.millis

    it should "Fail with RuntimeException on retrieving data from not available server" in {
        val httpService = new TestNotAvailableServiceHTTPService()
        httpService.getArticleData("http://notavailableserver", requestTimeout, 100).failed.futureValue shouldBe a [RuntimeException]
    }

    it should "Fail with RuntimeException on unknown response from" in {
        val httpService = new TestFailingServerHTTPService()
        httpService.getArticleData("http://notavailableserver", requestTimeout, 100).failed.futureValue shouldBe a [RuntimeException]
    }

    it should "Return CSV data on successful retrieval of articles" in {
        val httpService = new TestMockDataHTTPService()
        httpService.getArticleData("http://testserver", requestTimeout, 100).futureValue shouldBe a [String]
    }

    it should "Give Failure on not accepted result" in {
        val httpService = new TestNotAcceptingHTTPService()
        httpService.sendResult(Data.withoutHeaders, "http://testserver", 100, requestTimeout).futureValue shouldBe a [Failure[_]]
    }

    it should "Give Success on accepted result" in {
        val httpService = new TestSuccesfulHTTPService()
        httpService.sendResult(Data.validatedTen, "http://testserver", 100, requestTimeout).futureValue shouldBe a [Success[_]]
    }

    it should "Give Failure on different body response" in {
        val httpService = new TestSuccesfulHTTPServiceWithDifferentResponseBody()
        httpService.sendResult(Data.validatedTen, "http://testserver", 100, requestTimeout).futureValue shouldBe a [Failure[_]]    }

}