package mancvso.test

import scala.util.{Try, Success, Failure}
import org.scalatest.{FlatSpec, Matchers}

import mancvso.service.ParserService

class SerializationTests extends FlatSpec with Matchers {

  it should "deserialize a single register" in {
    val dataSingleLine = "A-UhnpVjCE|P-NhImbQSB|CKVTFO LCCOR TFIAZTP|lxqjlivf dppzKc|79.54|0"
    val res = ParserService.singleLine(dataSingleLine)
    res.size shouldEqual 1
    res.head shouldBe a [Success[_]]
  }

  it should "fail on bad formatted valid data" in {
    val dataSingleLine = "A-UhnpVjCE,P-NhImbQSB,CKVTFO LCCOR TFIAZTP,lxqjlivf dppzKc,79.54|0"
    val res = ParserService.singleLine(dataSingleLine)
    res.size shouldEqual 1
    res.head shouldBe a [Failure[_]]
  }

  it should "fail on null values" in {
    val dataSingleLine = null
    val res = ParserService.singleLine(dataSingleLine)
    res.size shouldEqual 1
    res.head shouldBe a [Failure[_]]
    val resMultiline = ParserService.multiLine(dataSingleLine)
    resMultiline.size shouldEqual 1
    resMultiline.head shouldBe a [Failure[_]]
  }

  it should "deserialize a batch of registries without header" in {    
    val res = ParserService.multiLine(Data.withoutHeaders, trim = true)
    res.size shouldEqual 6
    res.head shouldBe a [Success[_]]
  }

  it should "deserialize a batch of registries with header" in {
    val res = ParserService.multiLine(Data.five, trim = true, hasHeader = true)
    res.size shouldEqual 5
    res.head shouldBe a [Success[_]]
  }
  
  it should "fail on new schema" in {
    val res = ParserService.multiLine(Data.badSchema, trim = true, hasHeader = true)
    res.size shouldEqual 5
    res.head shouldBe a [Failure[_]]
  }

  it should "detect header" in {
    ParserService.hasHeader(Data.three) shouldEqual true
  }

  it should "have only one product id in a deserialization pass" in {
    val res = ParserService.multiLine(Data.thousand, trim = true, hasHeader = true)
      .filter( _.isSuccess )
      .map( e => e.get )
      .groupBy { case a => a.produktId.trim }
      .map( el => el._1 )

    res.size shouldEqual 27
  }

}